import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Billing from './views/Billing.vue'

Vue.use(Router)

export default new Router({
  base: '/',
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/Billing',
      name: 'billing',
      component: Billing
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
